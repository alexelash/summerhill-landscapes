<?php
/**
 * The template for displaying pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Summerhill_Landscapes
 */

get_header(); ?>
	<div class="site-container" role="main">
		<?php 
			while ( have_posts() ) : the_post();
				$slug 			= $post->post_name;
				$post_id 		= get_the_ID();
				$parent_id 	= wp_get_post_parent_id( $post_id );

				if ($parent_id == 7) { // A Project Page
					$prevURL = get_permalink(siblings(get_permalink($post_id))['before']->ID);
			    $nextURL = get_permalink(siblings(get_permalink($post_id))['after']->ID);
		      $parentURL = get_permalink(wp_get_post_parent_id( $post_id ));
		      $parentChildren = get_pages("child_of=".wp_get_post_parent_id( $post_id )."&sort_column=menu_order");
		      $firstchild = get_permalink($parentChildren[0]->ID);
		      $lastchild = get_permalink($parentChildren[count($parentChildren)-1]->ID);

		      if(siblings(get_permalink($post_id))['after']=="") $nextURL= $firstchild;
		      if(siblings(get_permalink($post_id))['before']=="") $prevURL= $lastchild;
					
					echo "<header class='page-navigation'>";
						echo "<div class='btn btn--next'><a href='$nextURL'>Next</a></div>";
					echo "</header>";
					get_template_part( 'template-parts/content', 'gallery' ); 
					get_template_part( 'template-parts/content', 'project_information' ); 
				} 
				else if ($parent_id == 9) { // A Child of Firm 
					get_template_part( 'template-parts/content', 'page' ); 
				}
				else if ($post_id == 10) { // Contact Page
					get_template_part( 'template-parts/content', 'page' ); 
				}
			endwhile; // End of the loop. 
		?>
	</div>
<?php get_footer(); ?>
