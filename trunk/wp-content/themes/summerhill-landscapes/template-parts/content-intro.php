<?php
/**
 * Template part for displaying GALLERY content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Summerhill_Landscapes
 */

?>
<div class="intro-container">
	<div class="intro">		
		<div class="piece piece--sans" id="piece-first">Appreciation of the landscape</div>
		<img class="piece piece--ampersand" id="piece-second" src="<?php echo get_template_directory_uri(); ?>/images/ampersand.png">
		<div class="piece piece--sans" id="piece-third">love of the east end</div>
	</div>
</div>
