<?php
/**
 * Template part for displaying Project content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Summerhill_Landscapes
 */

?>
<?php
	$page_title = get_the_title();
	$description = get_field('description');

	echo "<footer class='page-information-container'>";
		echo "<div class='page-information'>";
			echo "<h2 class='page-title'>$page_title</h2>";
			echo "<button class='btn btn--primary'>+</button>";
			echo "<div class='page-description'>$description</div>";
		 	echo "<aside class='controller'>";
		 		echo "<button class='btn btn--previous'>";
					get_template_part('images/btn', 'prev.svg');
		 		echo "</button>";
		 		echo "<div class='integer-container'>";
		 			echo "<span class='integer integer--numer'>1</span> / <span class='integer integer--denom'>0</span>";
		 			echo "</div>";
		 		echo "<button class='btn btn--next'>";
					get_template_part('images/btn', 'next.svg');
		 		echo "</button>";
		 	echo "</aside>";
		echo "</div>";
	echo "</footer>";
?>
