<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Summerhill_Landscapes
 */

?>
<?php 
	$page_color = get_field('color');
	$bg_image = get_field('image');
	$title = get_the_title();
	$slug = $post -> post_name;
	$bg_image_url = $bg_image['url'];
	$pullquote = get_field('pullquote');
	echo "<main class='site-content' style='background-color: $page_color;'>";
		echo "<div class='entry'>";
			echo "<header class='entry-header' style='background-image: url($bg_image_url);'></header>";
			echo "<div class='entry-content'>";
				echo "<h2 class='entry-title'>$title</h2>";
				echo "<blockquote class='entry-pullquote'>$pullquote</blockquote>";
				echo "<div class='entry-body'>";
					the_content();
				echo "</div>";
			echo "</div>";
		echo "</div>";
	echo "</main>";
?>

