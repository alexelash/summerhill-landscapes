<?php
/**
 * Template part for displaying GALLERY content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Summerhill_Landscapes
 */

?>
<?php 
	global $post;
	$slug = $post->post_name; 
	echo "<main class='site-content'>";
		echo "<div class='slideshow' id='$slug-slideshow'>";
			$images = get_field('images');
			$randomStart = rand(0, count($images));
			if( $images ){
				for ($i=0; $i < count($images); $i++) { 
					$modI = $i%count($images);
					$width = $images[$modI]['width'];
					$height = $images[$modI]['height'];
					$url = $images[$modI]['url'];
					$resizedUrl = aq_resize( $url, 1501);
					if ($resizedUrl=="") $resizedUrl = $iURL;
					$srcString = "data-src='$resizedUrl'";
					$loadClass = "delayed-load";
					if ($imgCount<2) {
						$srcString = "src='$resizedUrl'";
						$loadClass = "";
					}

					$alt = ($images[$modI]['alt'])?($images[$modI]['alt']):($images[$modI]['title']);
					$proportions = ($width > $height)?'wide':'tall';
					
					echo "<div class='slide' id='slide-$i'>";
						echo "<div class='image-holder $proportions $loadClass' style='background-image:url($resizedUrl);' data-width='$width' data-height='$height'>";
						echo "<img $srcString class='loader-image' alt='$alt' />";
						echo "</div>";
						echo "<img class='loading-anim' src='". get_template_directory_uri() ."/ring.svg'/>";
					echo "</div>";
				}
			}
		echo "</div>";
	echo "</main>";
?>

