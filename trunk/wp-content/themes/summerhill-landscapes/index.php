<?php
/**
 * Template Name: Homepage
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Summerhill_Landscapes
 */

get_header(); ?>
	<div class="site-container" role="main">
		<?php 
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', 'intro' );
				get_template_part( 'template-parts/content', 'gallery' );
			endwhile; // End of the loop. 
		?>
	</div><!-- #main -->

<?php get_footer(); ?>
