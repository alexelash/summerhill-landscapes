<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Summerhill_Landscapes
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php $metadescription = get_field('meta_description', 'option'); ?>
<meta name="description" content="<?php echo $metadescription?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="icon" href="<?php echo site_url() ?>/favicon.png">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo site_url() ?>/apple-touch-icon-57x57-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo site_url() ?>/apple-touch-icon-60x60-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo site_url() ?>/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo site_url() ?>/apple-touch-icon-76x76-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo site_url() ?>/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo site_url() ?>/apple-touch-icon-120x120-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo site_url() ?>/apple-touch-icon-144x144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="<?php echo site_url() ?>/apple-touch-icon-180x180-precomposed.png">

<script>
  // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  // ga('create', 'UA-13203844-35', 'auto');
  // ga('send', 'pageview');
</script>

<!-- Typekit, Open Sans -->
<script src="https://use.typekit.net/vzf2ucg.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header id="masthead" class="site-header" role="banner">
		<button class="btn btn--menu">Menu</button>
		<div class="site-branding">
			<?php 
				$site_url = site_url();
				echo "<h1 class='site-title'><a href='$site_url' rel='home'>";
					get_template_part('images/shl_logo', 'lrg.svg');
				echo "</a></h1>";
			?>
		</div><!-- .site-branding -->

		<nav class="main-navigation" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>

			<?php 
				$the_query = new WP_Query( 
					'page_id=10' 
				);
				while ( $the_query->have_posts() ) :
					$the_query->the_post();
					echo "<div class='contact-container'>";
						echo "<h2 class='contact-header'>";
							the_title();
						echo "</h2>";
		        the_content();
						endwhile;
						wp_reset_postdata();
						if( have_rows('social_media', 'option') ) {
						echo "<ul class='social-media'>";
						while( have_rows('social_media', 'option') ): the_row();
							$type = get_sub_field('type');
							$link = get_sub_field('url');
							//Facebook
							if ($type=='facebook') 	{
								$icon = '<svg enable-background="new 0 0 48 48" id="Layer_1" version="1.1" viewBox="0 0 48 48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><circle cx="24" cy="24" fill="#4E71A8" r="24"/><path d="M29.9,19.5h-4v-2.6c0-1,0.7-1.2,1.1-1.2c0.5,0,2.8,0,2.8,0v-4.4l-3.9,0c-4.4,0-5.3,3.3-5.3,5.3v2.9h-2.5V24  h2.5c0,5.8,0,12.7,0,12.7h5.3c0,0,0-7,0-12.7h3.6L29.9,19.5z" fill="#FFFFFF"/></svg><!--[if lt IE 9]><em>Facebook</em><![endif]-->';
								$displayTitle = 'Facebook';
							}
							// Twitter
							if ($type=='twitter') 	{
								$icon = '<svg enable-background="new 0 0 48 48" id="Layer_1" version="1.1" viewBox="0 0 48 48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><circle cx="24" cy="24" fill="#1CB7EB" r="24"/><g><g><path d="M36.8,15.4c-0.9,0.5-2,0.8-3,0.9c1.1-0.7,1.9-1.8,2.3-3.1c-1,0.6-2.1,1.1-3.4,1.4c-1-1.1-2.3-1.8-3.8-1.8    c-2.9,0-5.3,2.5-5.3,5.7c0,0.4,0,0.9,0.1,1.3c-4.4-0.2-8.3-2.5-10.9-5.9c-0.5,0.8-0.7,1.8-0.7,2.9c0,2,0.9,3.7,2.3,4.7    c-0.9,0-1.7-0.3-2.4-0.7c0,0,0,0.1,0,0.1c0,2.7,1.8,5,4.2,5.6c-0.4,0.1-0.9,0.2-1.4,0.2c-0.3,0-0.7,0-1-0.1    c0.7,2.3,2.6,3.9,4.9,3.9c-1.8,1.5-4.1,2.4-6.5,2.4c-0.4,0-0.8,0-1.3-0.1c2.3,1.6,5.1,2.6,8.1,2.6c9.7,0,15-8.6,15-16.1    c0-0.2,0-0.5,0-0.7C35.2,17.6,36.1,16.6,36.8,15.4z" fill="#FFFFFF"/></g></g></svg><!--[if lt IE 9]><em>Twitter</em><![endif]-->';
								$displayTitle = 'Facebook';
							}
							// Instagram
							if ($type=='instagram') {
								$icon = '<svg enable-background="new 0 0 48 48" id="Layer_1" version="1.1" viewBox="0 0 48 48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><circle cx="24" cy="24" fill="#444444" r="24"/><path d="M31.2,12.3H16.8c-2.5,0-4.5,2-4.5,4.5v4.8v9.6c0,2.5,2,4.5,4.5,4.5h14.4c2.5,0,4.5-2,4.5-4.5v-9.6v-4.8  C35.7,14.3,33.7,12.3,31.2,12.3z M32.5,15l0.5,0v0.5V19l-4,0l0-4L32.5,15z M20.7,21.6c0.7-1,2-1.7,3.3-1.7s2.6,0.7,3.3,1.7  c0.5,0.7,0.8,1.5,0.8,2.4c0,2.3-1.9,4.1-4.1,4.1s-4.1-1.8-4.1-4.1C19.9,23.1,20.2,22.3,20.7,21.6z M33.4,31.2c0,1.2-1,2.2-2.2,2.2  H16.8c-1.2,0-2.2-1-2.2-2.2v-9.6h3.5c-0.3,0.7-0.5,1.6-0.5,2.4c0,3.5,2.9,6.4,6.4,6.4c3.5,0,6.4-2.9,6.4-6.4c0-0.8-0.2-1.7-0.5-2.4  h3.5V31.2z" fill="#FFFFFF"/></svg><!--[if lt IE 9]><em>Instagram</em><![endif]-->';	
								$displayTitle = 'Instagram';
							}
							// LinkedIn
							if ($type=='linkedin') 	{
								$icon = '<svg enable-background="new 0 0 48 48" id="Layer_1" version="1.1" viewBox="0 0 48 48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><circle cx="24" cy="24" fill="#1686B0" r="24"/><path d="M17.4,34.9h-4.6V20.1h4.6V34.9z M14.9,18.2L14.9,18.2c-1.7,0-2.8-1.1-2.8-2.6c0-1.5,1.1-2.6,2.8-2.6  c1.7,0,2.8,1.1,2.8,2.6C17.7,17.1,16.7,18.2,14.9,18.2z M35.9,34.9h-5.3v-7.7c0-2-0.8-3.4-2.6-3.4c-1.4,0-2.1,0.9-2.5,1.8  c-0.1,0.3-0.1,0.8-0.1,1.2v8h-5.2c0,0,0.1-13.6,0-14.8h5.2v2.3c0.3-1,2-2.5,4.6-2.5c3.3,0,5.9,2.1,5.9,6.7V34.9z" fill="#FFFFFF"/></svg><!--[if lt IE 9]><em>LinkedIn</em><![endif]-->';
								$displayTitle = 'LinkedIn';
							}
							// Pinterest
							if ($type=='pinterest') {
								$icon = '<svg enable-background="new 0 0 48 48" id="Layer_1" version="1.1" viewBox="0 0 48 48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><circle cx="24" cy="24" fill="#CA3737" r="24"/><path d="M24,12.1c-6.6,0-11.9,5.3-11.9,11.9c0,4.9,2.9,9,7.1,10.9c0-0.8,0-1.8,0.2-2.7c0.2-1,1.5-6.5,1.5-6.5  s-0.4-0.8-0.4-1.9c0-1.8,1-3.1,2.3-3.1c1.1,0,1.6,0.8,1.6,1.8c0,1.1-0.7,2.7-1,4.2C23.1,28,24.1,29,25.3,29c2.3,0,3.8-2.9,3.8-6.3  c0-2.6-1.8-4.5-4.9-4.5c-3.6,0-5.8,2.7-5.8,5.7c0,1,0.3,1.8,0.8,2.3c0.2,0.3,0.3,0.4,0.2,0.7c-0.1,0.2-0.2,0.7-0.2,1  c-0.1,0.3-0.3,0.4-0.6,0.3c-1.7-0.7-2.4-2.5-2.4-4.5c0-3.4,2.8-7.4,8.5-7.4c4.5,0,7.5,3.3,7.5,6.8c0,4.7-2.6,8.1-6.4,8.1  c-1.3,0-2.5-0.7-2.9-1.5c0,0-0.7,2.7-0.8,3.3c-0.3,0.9-0.7,1.8-1.2,2.5c1.1,0.3,2.2,0.5,3.4,0.5c6.6,0,11.9-5.3,11.9-11.9  C35.9,17.4,30.6,12.1,24,12.1z" fill="#FFFFFF"/></svg><!--[if lt IE 9]><em>Pinterest</em><![endif]-->';
								$displayTitle = 'Pinterest';
							}
							// Google+
							if ($type=='google-plus') {
								$icon = '<svg enable-background="new 0 0 48 48" id="Layer_1" version="1.1" viewBox="0 0 48 48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><circle cx="24" cy="24" fill="#E3411F" r="24"/><g><path d="M24.8,25.8c-0.7-0.5-2-1.7-2-2.3c0-0.8,0.2-1.2,1.4-2.2c1.2-1,2.1-2.3,2.1-3.9c0-1.9-0.8-3.7-2.4-4.4h2.4   l1.7-1.2h-7.5c-3.4,0-6.6,2.6-6.6,5.5c0,3,2.3,5.5,5.7,5.5c0.2,0,0.5,0,0.7,0c-0.2,0.4-0.4,0.9-0.4,1.4c0,0.8,0.5,1.5,1,2.1   c-0.4,0-0.9,0-1.3,0c-4.2,0-7.4,2.7-7.4,5.4c0,2.7,3.5,4.4,7.7,4.4c4.8,0,7.4-2.7,7.4-5.4C27.4,28.6,26.8,27.2,24.8,25.8z    M20.7,22.1c-1.9-0.1-3.8-2.2-4.1-4.7c-0.3-2.6,1-4.5,2.9-4.4c1.9,0.1,3.8,2.1,4.1,4.7C24,20.1,22.7,22.1,20.7,22.1z M20,35   c-2.9,0-5-1.8-5-4c0-2.2,2.6-4,5.5-3.9c0.7,0,1.3,0.1,1.9,0.3c1.6,1.1,2.7,1.7,3,3c0.1,0.3,0.1,0.5,0.1,0.8C25.5,33.2,24,35,20,35z   " fill="#FFFFFF"/><polygon fill="#FFFFFF" points="32.8,22.3 32.8,19.4 30.4,19.4 30.4,22.3 27.5,22.3 27.5,24.6 30.4,24.6 30.4,27.6 32.8,27.6    32.8,24.6 35.7,24.6 35.7,22.3  "/></g></svg><!--[if lt IE 9]><em>Facebook</em><![endif]-->';
								$displayTitle = 'Google+';
							}
							// Youtube
							if ($type=='youtube') {
								$icon = '<svg enable-background="new 0 0 48 48" id="Layer_1" version="1.1" viewBox="0 0 48 48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><circle cx="24" cy="24" fill="#CA3737" r="24"/><path d="M35.2,18.5c0-0.1,0-0.2-0.1-0.3c0,0,0-0.1,0-0.1c-0.3-0.9-1.1-1.5-2.1-1.5h0.2c0,0-3.9-0.6-9.2-0.6  c-5.2,0-9.2,0.6-9.2,0.6H15c-1,0-1.8,0.6-2.1,1.5c0,0,0,0.1,0,0.1c0,0.1,0,0.2-0.1,0.3c-0.1,1-0.4,3.1-0.4,5.5  c0,2.4,0.3,4.5,0.4,5.5c0,0.1,0,0.2,0.1,0.3c0,0,0,0.1,0,0.1c0.3,0.9,1.1,1.5,2.1,1.5h-0.2c0,0,3.9,0.6,9.2,0.6  c5.2,0,9.2-0.6,9.2-0.6H33c1,0,1.8-0.6,2.1-1.5c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0.1-0.3c0.1-1,0.4-3.1,0.4-5.5  C35.6,21.6,35.4,19.5,35.2,18.5z M27.4,24.5l-4.7,3.4C22.6,28,22.5,28,22.4,28c-0.1,0-0.2,0-0.3-0.1c-0.2-0.1-0.3-0.3-0.3-0.5v-6.8  c0-0.2,0.1-0.4,0.3-0.5c0.2-0.1,0.4-0.1,0.6,0l4.7,3.4c0.1,0.1,0.2,0.3,0.2,0.5C27.7,24.2,27.6,24.4,27.4,24.5z" fill="#FFFFFF"/></svg><!--[if lt IE 9]><em>YouTube</em><![endif]-->';
								$displayTitle = 'YouTube';
							}
							// Houzz
							if ($type=='houzz') {
								$icon = '<svg x="0px" y="0px" viewBox="0 0 11.8 11.8" style="enable-background:new 0 0 11.8 11.8;" xml:space="preserve"><style type="text/css"></style><circle class="st0" cx="5.9" cy="5.9" r="5.9"/><g><polygon points="4,8.1 5.9,6.9 4,5.9 	"/><polyline points="5.9,4.8 5.9,2.6 4,3.8 4,5.9 	"/><polygon points="5.9,9.1 7.7,8 7.7,5.9 5.9,6.9 	"/><polygon points="5.9,4.8 7.7,5.9 7.7,3.7 	"/></g></svg><!--[if lt IE 9]><em>Houzz</em><![endif]-->';
								$displayTitle = 'Houzz';
							}
							echo "<li class='$type social-media-item'>";
								echo "<a class='social-media-link' href='$link' target='_blank' title='$displayTitle'>$icon</a>";
							echo "</li>";
						endwhile;
					echo "</ul>";
	   		}
      echo "</div>";		
	 	?>
		</nav><!-- .navigation -->
	</header><!-- #masthead -->
