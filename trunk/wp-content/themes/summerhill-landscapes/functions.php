<?php
/**
 * Summerhill Landscapes functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Summerhill_Landscapes
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}
function pp($a,$b = '') {
	echo '<pre>'.print_r($a,1).'<br/>'.$b.'</pre>';
}
require_once('aq_resizer.php');
if ( ! function_exists( 'summerhill_landscapes_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function summerhill_landscapes_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Summerhill Landscapes, use a find and replace
	 * to change 'summerhill-landscapes' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'summerhill-landscapes', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'summerhill-landscapes' ),
	) );


	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'summerhill_landscapes_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // summerhill_landscapes_setup
add_action( 'after_setup_theme', 'summerhill_landscapes_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function summerhill_landscapes_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'summerhill_landscapes_content_width', 640 );
}
add_action( 'after_setup_theme', 'summerhill_landscapes_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function summerhill_landscapes_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'summerhill-landscapes' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'summerhill_landscapes_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function summerhill_landscapes_scripts() {
	wp_enqueue_style( 	'summerhill-landscapes-style', get_stylesheet_uri() );
	wp_enqueue_script( 	'summerhill-landscapes-plugins', get_template_directory_uri() . '/js/plugins.js');
	wp_enqueue_script( 	'summerhill-landscapes-main', get_template_directory_uri() . '/js/main.js');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'summerhill_landscapes_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

function siblings($link) {
    global $post;
    $siblings = get_pages('child_of='.$post->post_parent.'&parent='.$post->post_parent.'&sort_column=menu_order');
    foreach ($siblings as $key=>$sibling){
        if ($post->ID == $sibling->ID){
            $ID = $key;
        }
    }
    $closest = array('before'=>$siblings[$ID-1],'after'=>$siblings[$ID+1]);

    return $closest;
}

function dyad_credit(){
?>
	<style type="text/css" media="screen">
		@font-face {
			font-family: "MrsEaves";
			src: url('<?php echo get_template_directory_uri();?>/webfonts/mrseaves/MrsEaves-rom.woff') format('woff');
			src: url('<?php echo get_template_directory_uri();?>/webfonts/mrseaves/MrsEaves-rom.eot');
			/*src: url('<?php echo get_template_directory_uri();?>/webfonts/mrseaves/MrsEaves-rom.eot') format('eot');*/
			font-weight: normal;
			font-style: normal;
		}

		@font-face {
			font-family: "MrsEaves";
			src: url('<?php echo get_template_directory_uri();?>/webfonts/mrseaves/MrsEaves-ita.woff') format('woff');
			src: url('<?php echo get_template_directory_uri();?>/webfonts/mrseaves/MrsEaves-ita.eot');
			/*src: url('<?php echo get_template_directory_uri();?>/webfonts/mrseaves/MrsEaves-rom.eot') format('eot');*/
			font-weight: normal;
			font-style: italic;
		}
		#credit{
			display:none;
		}
		#credit{
			margin: 0;	
			padding: 0;		
			text-align: center;
			
			line-height: 2;
			font-family: MrsEaves;
			font-style: normal;
			overflow: hidden;
			font-size: 0.75em;
		  visibility: hidden;
			text-align: center;
			display: inline-block;
			opacity: 0.75;
			z-index: 9999;
			color: #ADADAD!important;
	    position: absolute;
	    bottom: 1.65rem;
	    right: 2rem;
	    /*left: 1rem;*/
			
			width: auto;
			text-decoration: none;
	    -webkit-font-smoothing: antialiased;
	  	-moz-osx-font-smoothing: grayscale;
		}
		#credit #dyad{
			text-transform: uppercase;
			/*color: #bcbcb7;*/
		}
		#credit span#holder{
			white-space:nowrap;
		}
		/*	#therest, #siteby {
				display: none;
			}*/
			/*.touch #therest, .touch #siteby {
				display: inline-block;
				
			}*/
		}
</style>
<script type="text/javascript" charset="utf-8">
	window.onload = function() {
		initDyadCredit();
		function initDyadCredit(){
      setTimeout("$('#credit').css('visibility','visible');",2200);
			// if(!('ontouchstart' in document.documentElement)) 
			hideCredit( $('#credit'), 0);
		

			$('#credit').hover(function(){
				revealCredit( $(this), .5);
			},function(){
				hideCredit( $(this), .5);
			});
	
			function hideCredit(c,dur){
				// if('ontouchstart' in document.documentElement) return; 
				TweenLite.to(c, dur, {width:c.find('#d').width()});
				TweenLite.to(c.find('#therest'), dur, {opacity:0});
				TweenLite.to(c.find('span#holder'), dur, {marginLeft:-1*c.find('span#siteby').width()});
			}
	
			function revealCredit(c,dur){
				if('ontouchstart' in document.documentElement) return; 
				TweenLite.to(c, dur, {width:c.find('span#holder').width()});
				TweenLite.to(c.find('#therest'), dur, {opacity:1});
				TweenLite.to(c.find('span#holder'), dur, {marginLeft:0});
			}
		}
	};
</script>
<a id="credit" href="http://www.dyadcom.com" target="_blank"><span id="holder"><span id="siteby"><em>Site by</em> </span><span id="dyad"><span id="d">D</span><span id="therest">yad Communications</span></span></span></a>
<?php
}

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
