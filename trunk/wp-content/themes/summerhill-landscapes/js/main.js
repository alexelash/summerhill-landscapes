var isIntro,
    isSmScreen = $(window).width()<680,
    isTouch = $('html').hasClass('touch'),
    isSkip,
    iOS = /iPad|iPhone|iPod/.test(navigator.platform);

$(document).ready(onReady);

function onReady() {
	History.Adapter.bind(window,'statechangecomplete',function(e, theNewBody){
		// console.log('statechangecomplete');
	});

	History.Adapter.bind(window,'contentswitchcomplete',function(e, theNewContent){
		onPageChange();
	});
	activateNav();
	onPageChange();
}

function fallBack() {
	var a = document.querySelector('.site-title .title');
	if ($('body').hasClass('home') && !$('body').hasClass('menuOpen') ) {
		a.setAttribute('transform', 'translate(0, 0) scale(1)');
	} else {
		a.setAttribute('transform', 'translate(-3, 65) scale(0.65)');
	}
}

function onPageChange() {
	isSkip = false;
	$('body').removeClass('home');
	if ($('#home-slideshow').length) {
		$('body').addClass('home');
		if (isSmScreen) isSkip = true;
		initIntro(isSkip);
	};
	initCycle($('#home-slideshow'))
	if (!isTouch && $('.slideshow')) initCycle($('.slideshow'));
	else if (isTouch && $('.slideshow:not(#home-slideshow)')) initSwipe($('.slideshow:not(#home-slideshow)'));

	if ($('.page-information-container')) initDesc($('.page-description'));
	imagesLoaded($('img.loader-image'));
	$('body, .main-navigation').removeClass('menuOpen');
	$('.btn--menu').removeClass('clicked').text('Menu');
	if ($('html').hasClass('no-cssreflections')) fallBack();
}

function initSwipe(slideshow){
	slideshow.append( '<div class="swipe-instructional">Swipe to view project</div>' );
	var currentImg = 0;
	slideshow.delay(1000).fadeIn(1000);
	slideshow.addClass('swiping');
	slideshow.width($(window).width());
	var IMG_WIDTH = slideshow.width();
	$(window).resize(function(){
		slideshow.width($(window).width());
		var IMG_WIDTH = slideshow.width();
	});
	var imgs = slideshow.find('.slide'),
			numImages=imgs.length,
			speed=500;
	$('.integer--denom').html(numImages);
	imgs.width(IMG_WIDTH).css('float','left').height($(window).height());
	slideshow.width(numImages*IMG_WIDTH);
	$(window).resize(function(){
		imgs.width(IMG_WIDTH).css('float','left').height($(window).height());
		slideshow.width(numImages*IMG_WIDTH);
	});

	var swipeOptions={
		triggerOnTouchEnd: true,
		swipeStatus: swipeStatus,
		threshold: 75
	};
	if(currentImg!=0) scrollImages( IMG_WIDTH * currentImg, 0);
	else updateImage(slideshow, 'right');

	imgs.swipe( swipeOptions );

	function swipeStatus(event, phase, direction, distance){
		if( phase=="move" && (direction=="left" || direction=="right") ){
			scrollImages((IMG_WIDTH * currentImg) + (direction == "left"?distance:-1*distance), 0);
			$('.swipe-instructional').fadeOut();
		}
		else if ( phase == "cancel") scrollImages(IMG_WIDTH * currentImg, speed);
		else if ( phase =="end" ) updateImage(slideshow,direction); 
	}

	function updateImage(slideshow, dir){
		currentImg = (dir == "right" ? Math.max(currentImg-1, 0) : Math.min(currentImg+1, numImages-1));
		for (var i = imgs.length - 1; i >= 0; i--) {
			if(2>Math.abs(i-currentImg)) $(imgs[i]).removeClass('showing');
			else $(imgs[i]).addClass('showing');
			$('.integer--numer').html(currentImg+1);
		}
		if(currentImg>0 || numImages<2) $('.cycle-title span, .swipe-instructional').hide();
		slideshow.find('.slide').removeClass('showing');
		slideshow.find('.slide').eq(currentImg).addClass('showing');
		scrollImages( IMG_WIDTH * currentImg, speed);
	}

	function scrollImages(distance, duration){
		imgs.css("-webkit-transition-duration", (duration/1000).toFixed(1) + "s");
		var value = (distance<0 ? "" : "-") + Math.abs(distance).toString();
		imgs.css("-webkit-transform", "translate3d("+value +"px,0px,0px)");
	}
}

function imagesLoaded(theImage){
	theImage.imagesLoaded().progress( function( instance, image ) {
		$(image.img).parent().addClass('isLoaded');
	});
}

function activateNav(){
	$('.btn--menu').on('click', function(e) {
		$('body, .main-navigation').toggleClass('menuOpen');
		$(this).toggleClass('clicked').text('×');
		if ($(this).hasClass('clicked')) $(this).text('×');
		else $(this).text('Menu');
		if ($('html').hasClass('no-cssreflections')) fallBack();
	});
}

function closeMenu() {

}

function initDesc(description) {
	$('.btn--primary').on('click', function(){
		description.parent().toggleClass('descOpen');
		if (description.parent().hasClass('descOpen')) $(this).html('-');
		else $(this).html('+');
	});
	$('.btn--prev, .btn--next, .slideshow').on('click', function() {
		if (description.parent().hasClass('descOpen')) description.parent().removeClass('descOpen');
	});
}

function initCycle(slideshow){
	slideshow.each(function(){
		$(this).cycle({
			 slides: ".slide",
			 timeout: 3000,
			 manualSpeed: 800,
			 speed: 3000,
			 fx: 'fadeout',
			 prev: 'footer .btn--previous',
			 next: ".slide, footer .btn--next",
			 paused: true,
			 log: false
		});
		$(document).keyup(function (e) {
			if (e.keyCode == 37) slideshow.cycle('prev');
			else if (e.keyCode == 39) slideshow.cycle('next');
		});
		slideshow.on( 'cycle-before', function( event, opts , outgoingSlideEl, incomingSlideEl, forwardFlag) {
			var currentSlideIndex = slideshow.find('.cycle-slide:not(.cycle-sentinel)').index($(incomingSlideEl));
			for (var i = currentSlideIndex-2; i < currentSlideIndex+2; i++) {
				var futureSlideImg = slideshow.find('.cycle-slide:not(.cycle-sentinel)').eq(i).find('img.loader-image');
	      futureSlideImg.attr('src', futureSlideImg.data('src')).css('width','auto');
	      // futureSlideImg.imagesLoaded().progress( function( instance, image ) {
	      //     $(image.img).parent().addClass('isLoaded');

	      // });
				// console.log(futureSlideImg);
				imagesLoaded(futureSlideImg);
			}
		});
	});

	initNum(slideshow);
}

function initNum(slideshow){
	var slides = slideshow.find('.slide'),
			numImages = slides.length;
	// $('#denom').html(slides.find('.slide').length);

	slideshow.on('cycle-before', onSlideshowChange);
	$('.integer--denom').html(numImages);

	function onSlideshowChange(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag){
		$('.integer--numer').html(optionHash.nextSlide+1);
	}
}

function initIntro(isSkip) {
	if (isSkip == false) {
		$('.piece--sans').each(function(){
			chars = $(this).html();
			chars = chars.split("");
			$(this).html('<span>'+chars.join('</span><span>')+'</span>');
			
			$('.piece--sans').delay(2000).queue(function(next){
				$('.piece--sans').find('span').each(function(index) {
					$(this).delay(50*index).queue(function(next){
						$(this).addClass('show');
						next();
					});
				});
				next();
			});
		});
		$('.intro-container').find('.piece--ampersand').delay(3000).queue(function(next) {
			$(this).addClass('show');
			next();
		});
		setTimeout(introEnd,4000);
	}
	setTimeout(introEnd,0);
}

function introEnd() {
	$('body').addClass('introEnd');
	if ($('body').hasClass('introEnd')) $('.home .slideshow').delay(1000).cycle('resume');
}
